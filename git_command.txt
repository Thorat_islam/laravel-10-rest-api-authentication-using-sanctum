git config --global user.name "Thorat Islam"
git config --global user.email "mdthoratislam1993.oni@gmail.com"

Create a new repository

git clone https://gitlab.com/Thorat_islam/laravel-10-rest-api-authentication-using-sanctum.git
cd laravel-10-rest-api-authentication-using-sanctum
git switch --create main
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin main

Push an existing folder

cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/Thorat_islam/laravel-10-rest-api-authentication-using-sanctum.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main

Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/Thorat_islam/laravel-10-rest-api-authentication-using-sanctum.git
git push --set-upstream origin --all
git push --set-upstream origin --tags
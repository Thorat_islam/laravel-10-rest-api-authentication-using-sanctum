<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <style>
        * {
            box-sizing: border-box;
        }
        body {
            background: darkgrey;
        }
        h2 {
            text-align: center;
            font-family: Arial;
        }
        .beacon {
            box-sizing: border-box;
            position: relative;
            margin: 50px auto;
            width: 200px;
            height: 200px;
            transform-origin: 50% 50%;
            transform-style: preserve-3d;
            /* This is important! for z-axis effects */
            perspective: 120px;
        }
        .beam-circle {
            position: absolute;
            width: 1px;
            height: 1px;
            border-radius: 50%;
            left: 50%;
            top: 50%;
            transform-style: preserve-3d;
            transform-origin: center center;
            transform: translate(-50%, -50%);
            animation: 5s rotate linear infinite;
            box-shadow: 0 0 25px 25px #FFFFFF, 0 0 25px 25px whitesmoke;
        }
        .beam-triangle {
            position: absolute;
            right: 0%;
            top: 50%;
            width: 100px;
            height: 50px;
            border-left: 0px solid transparent;
            border-top: 25px solid transparent;
            border-right: 100px solid floralwhite;
            border-bottom: 25px solid transparent;
            transform-origin: center left;
            opacity: .1;
            transform: translate(0%,-50%);
            animation: 5s rotate2 linear infinite;
            filter: blur(4px);
        }
        .light {
            position: absolute;
            left: 50%;
            top: 50%;
            height: 60px;
            width: 40px;
            /*background: rgb(0,0,200);*/
            background: lightcyan;
            border-radius: 20px 20px 50% 50% / 20px 20px 15% 15%;
            border-bottom: 2px solid #3C510C;
            transform: translate(-50%,-50%);
            overflow: hidden;
            animation: 5s glow linear infinite;

        }
        .light-front {
            position: absolute;
            left:0;
            top:0;
            height: 60px;
            width: 40px;
            background: cornflowerblue;
            box-shadow: inset 5px 3px 15px #3b5998,  inset -5px 3px 15px #3b5998;
        }
        .light-spinner {
            position: absolute;
            left:0;
            top:0;
            height: 60px;
            width: 40px;
            background: #000;
            border-radius: 20px 20px 50% 50% / 20px 20px 15% 15%;
            animation: 5s rotate3 linear infinite;
        }
        .light-bulb {
            position: absolute;
            width: 4px;
            height: 4px;
            top:28px;
            left:18px;
            /*background: rgba(50,50,255,1);*/
            background: #fbed50;
            box-shadow: 0 0 3px 15px rgba(200,200,100,.2);
            border-radius: 50%;
            z-index: 1;
            animation: 5s rotate3 linear infinite, 5s flash linear infinite;
            backface-visibility: hidden;
        }

        .light-base {
            display: block;
            position: absolute;
            height: 30px;
            width: 50px;
            bottom:45px;
            top: 125px;
            left: 50%;
            background: black;
            box-shadow: inset 5px 0 10px black,  inset -5px 0 10px black;
            border-radius: 50% 50% 50% 50% / 50% 50% 20% 20%;
            transform: translate(-50%,-50%);
        }

        @keyframes rotate {
            0% {
                transform: translate(-50%,-50%) rotateY(0deg) translateZ(100px);
            }
            100% {
                transform: translate(-50%,-50%) rotateY(360deg) translateZ(100px);
            }
        }
        @keyframes rotate2 {
            0% {
                transform: rotateY(270deg) translate(0%,-50%) rotateY(0deg) translateZ(0px);
            }
            100% {
                transform: rotateY(270deg) translate(0%,-50%) rotateY(360deg) translateZ(0px);
            }
        }
        @keyframes rotate3 {
            0% {
                transform: rotateY(0deg);
            }
            100% {
                transform:  rotateY(360deg);
            }
        }
        @keyframes flash {
            0%, 100% {
                /*box-shadow: 0 0 3px 15px rgba(100,100,255,.3);*/
                box-shadow: 0 0 3px 15px whitesmoke;
            }
            50% {
                box-shadow: 0 0 0px 0px rgba(100,100,255,0);
            }
        }
        @keyframes glow {
            25% {
                /*box-shadow: 10px -5px 20px  rgba(100,100,255,.9);*/
                box-shadow: 10px -5px 20px  whitesmoke;
            }
            0%,100% {
                /*box-shadow: 0 -5px 20px rgba(100,100,255,0.95);*/
                box-shadow: 0 -5px 20px whitesmoke;
            }
            50% {
                /*box-shadow: 0 -5px 20px rgba(100,100,255,0.9);*/
                box-shadow: 0 -5px 20px whitesmoke;
            }
            75% {
                /*box-shadow: -10px -5px 20px  rgba(100,100,255,0.9);*/
                box-shadow: -10px -5px 20px  whitesmoke;
            }
        }
    </style>
</head>
<body >
<h2>Api Provide...</h2>
<div class="beacon">
    <div class="light-base"></div>
    <div class="light">
        <div class="light-bulb"></div>
        <div class="light-spinner"></div>
        <div class="light-front"></div>
    </div>
    <div class="beam-circle"></div>
    <div class="beam-triangle"></div>

    <audio id="audio">
        <source src="{{asset('assetes/audio/police-version-1.mp3')}}" type="audio/mpeg">
    </audio>
</div>
<script>
    window.onload = function()
    {
        var el = document.querySelector('.beacon'),
            audio = document.querySelector('#audio');
        el.addEventListener('mouseover', function(e) {
            audio.play();
        }, false);
        el.addEventListener('mouseout', function(e) {
            audio.pause();
        }, false);
    }
</script>
</body>
</html>

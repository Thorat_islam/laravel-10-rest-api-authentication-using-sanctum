<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\v1\TaskController;
use App\Http\Controllers\api\v1\CompleteTaskController;


Route::middleware('auth:sanctum')->prefix('v1')->group(function () {
    //Route::get('/tasks', [TaskController::class, 'index'])->name('tasks.index'); // Custom index route
    Route::apiResource('/tasks',TaskController::class);
    Route::patch('/tasks/{task}/complete',CompleteTaskController::class);

});
?>

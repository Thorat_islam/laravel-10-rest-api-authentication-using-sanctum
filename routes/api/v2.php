<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\v2\TaskController;
use App\Http\Controllers\api\v2\CompleteTaskController;


Route::middleware('auth:sanctum')->prefix('v2')->group(function () {
    //Route::get('/tasks', [TaskController::class, 'index'])->name('tasks.index'); // Custom index route
    Route::apiResource('/tasks',TaskController::class);
    Route::patch('/tasks/{task}/complete',CompleteTaskController::class);

});

?>
